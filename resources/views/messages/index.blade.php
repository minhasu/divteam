@extends('layout')

@section('content')
<div class="content row">
  @foreach ($message_infos as $message_info)
  <dl class="clearfix">
    <dt>{{ $message_info['usered_name'] }} さんへ</dt>
    <dd>
      {{ $message_info['message'] }}
      <span>{{ $message_info['point'] }}賞賛ポイントをあげました！</span>
    </dd>
    <dd><a href="/users/{{ $message_info['user_id'] }}">{{ $message_info['user_name'] }}より</a></dd>
  </dl>
  @endforeach
</div>
@endsection
