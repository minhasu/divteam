@extends('layout')

@section('content')
    <div class="contents row">
        <div class="success">
            <h3>賞賛しました！</h3>
            <a class="btn" href="/messages">賞賛一覧へ戻る</a>
        </div>
    </div>
@endsection
