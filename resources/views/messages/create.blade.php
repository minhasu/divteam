@extends('layout')

@section('content')
<div class="contents row">
  <div class="container">
    {{ Form::open(['url' => '/messages']) }}
    <h3>賞賛する</h3>
    <p>誰に送る？</p>
    <select class="" name="select">
        <option name="" value="" disabled selected style='display:none;'>誰に送る？</option>
      @foreach($users as $user)
      @if ($user->id != Auth::user()->id)
        <option name="who_id" value="{{ $user->id }}">{{ $user->name }}</option>
      @endif
      @endforeach
    </select>
    <p>メッセージ内容</p>
    <textarea cols="30" name="message" placeholder="ex) 感謝したことなど" rows="10"></textarea>
    <p>何ポイント送る？<span>（残ポイント：{{ Auth::user()->points }}P）</span></p>
    <input type="text" name="point">
    <input type="submit" name="" value="送る">
    {{ Form::close() }}
  </div>
</div>
@endsection
