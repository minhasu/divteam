@extends('layout')

@section('content')
  <div class="contents row">
    <p>{{ $user_info->name }}さんの褒められた履歴</p>
    @foreach($given_messages as $given_message)
        <div class="content_post">
            <h3>{{ $given_message->user_name }} さんから {{ $user_info->name }} さんへ</h3>
            <p>{{ $given_message->message }}</p>
            <h4>{{ $given_message->point }}賞賛ポイントをもらいました！</h4>
        </div>
    @endforeach
  </div>
@endsection
