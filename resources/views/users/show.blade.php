@extends('layout')

@section('content')
  <div class="contents row">
    <p>{{ $user_info->name }}さんの保有ポイント： {{ $user_info->points }}ポイント</p>
    <p>{{ $user_info->name }}さんの所属しているチーム：</p>
    <ul>
      @foreach ($teams as $team)
      <li>{{ $team->team_name }}</li>
      @endforeach
    </ul>
    <p class="users-top clearfix">{{ $user_info->name }}さんの褒め履歴<span style="float:right;"><a href="/users/{{ $user_info->id }}/given">＞{{ $user_info->name }}さんへの褒め履歴</a></span></p>
    @foreach($user_messages as $user_message)
        <div class="content_post">
            <h3>{{ $user_message->who_name }} さんへ</h3>
            <p>{{ $user_message->message }}</p>
            <h4>{{ $user_message->point }}賞賛ポイントをあげました！</h4>
        </div>
    @endforeach
    {{ $user_messages->render() }}
  </div>
@endsection
