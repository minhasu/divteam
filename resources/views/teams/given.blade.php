@extends('layout')

@section('content')
<div class="contents row">
  <div class="success">
    <h3>チームを賞賛しました！</h3>
    <a class="btn" href="/teams">チーム一覧へ戻る</a>
  </div>
</div>
@endsection
