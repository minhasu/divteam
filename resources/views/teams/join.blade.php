@extends('layout')

@section('content')
<div class="contents row">
  <div class="container">
    {{ Form::open(['url' => '/teams/'. $team->team_id .'/joined']) }}
    <h3>「{{ $team->team_name }}」チームに加わる</h3>
    <p style="text-align:center;">このチームに加わりますか？</p>
    <input type="hidden" name="team_id" value="{{ $team->team_id }}">
    <input type="submit" name="" value="じょいんぬする！">
    {{ Form::close() }}
  </div>
</div>
@endsection
