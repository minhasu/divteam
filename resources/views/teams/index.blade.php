@extends('layout')

@section('content')
  <div class="contents row" >
    <div class="teams-head clearfix">
      <h1>チーム一覧画面</h1>
      <ul class="clearfix">
        <li><a href="/teams/create">チームを作成する</a></li>
      </ul>
    </div>
    @foreach($teams as $team)
      <div class="content_post">
        <p><a href="/teams/{{ $team->team_id }}">{{ $team->team_name }} チーム</a></p>
        <a href="/teams/{{ $team->team_id }}/join">このチームに加わる</a>
        <a href="/teams/{{ $team->team_id }}/give">このチームを賞賛する</a>
      </div>
    @endforeach
    {{ $teams->render() }}
  </div>
@endsection
