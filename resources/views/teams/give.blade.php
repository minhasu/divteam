@extends('layout')

@section('content')
<div class="contents row">
  <div class="container">
    {{ Form::open(['url' => '/teams/'. $team->team_id .'/given']) }}
    <h3>「{{ $team->team_name }}」チームを賞賛する</h3>
    <input type="hidden" name="team_id" value="{{ $team->team_id }}">
    <textarea cols="30" name="message" placeholder="メッセージ" rows="10"></textarea>
    <input type="text" name="point" placeholder="何ポイント送る？">
    <input type="submit" name="" value="送る">
    {{ Form::close() }}
  </div>
</div>
@endsection
