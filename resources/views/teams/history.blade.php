@extends('layout')

@section('content')
  <div class="contents row" >
    <div class="teams-head clearfix">
      <h1>{{ $team_name }}チームの賞賛履歴</h1>
      <h2><a href="/teams">チーム一覧に戻る</a></h2>
    </div>
    @foreach($team_messages as $team_message)
    <div class="clearfix">
      <p>{{ $team_message->user_name }} さんより</p>
      <p>{{ $team_message->message }}</p>
      <p>{{ $team_message->point }}賞賛ポイントをもらいました！</span></p>
    </div>
    @endforeach
  </div>
@endsection
