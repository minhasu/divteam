@extends('layout')

@section('content')
  <div class="contents row">
    <h1>チーム名：{{ $team->team_name }}</h1>
    <h2>リーダー名：{{ $team->leader_name }}</h2>
    <h3><a href="/teams/{{ $team->team_id }}/history">このチームが賞賛された履歴</a></h3>
    <p><a href="/teams/{{ $team->team_id }}/join">このチームに加わる</a></p>
    <p><a href="/teams/{{ $team->team_id }}/give">このチームを賞賛する</a></p>
    <div class="content_post">
        <h3>チームメンバー：</h3>
        @foreach ($members as $member)
        <p>{{ $member->name }}</p>
        @endforeach
    </div>
    <p><a href="/teams">一覧に戻る</a></p>
  </div>
@endsection
