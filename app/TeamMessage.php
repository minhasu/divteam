<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamMessage extends Model
{
    //
    protected $fillable = [
        'user_id', 'team_id', 'point', 'message'
    ];

    public function user()
    {
      return $this->hasMany(User::class);
    }

    public function team()
    {
      return $this->hasMany(Team::class);
    }

}
