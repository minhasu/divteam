<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Team;
use App\TeamMessage;
use App\TeamMember;

class TeamsController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth', array('except' => 'index'));
    }

    public function index()
    {
      $teams = Team::groupBy('team_id')
        ->orderBy('updated_at', 'DESC')
        ->paginate(20);

      return view('teams.index')->with('teams', $teams);
    }

    public function create()
    {
      $users = User::all();

      return view('teams.create')->with('users', $users);
    }

    public function store(Request $request)
    {
      $create_team = Team::create([
        'team_name'   => $request->team_name,
        'leader_id'   => $request->select,
        'leader_name' => $request->leader_name
      ]);
      $create_team->save();

      TeamMember::create([
        'team_id' => $create_team->id,
        'user_id' => $request->select
      ]);

      return view('teams.store');
    }

    public function show($team_id)
    {
      $team = Team::where('team_id', $team_id)->first();
      $member_ids = TeamMember::where('team_id', $team_id)->get();
      $members = [];
      foreach ($member_ids as $member_id) {
        $members[] = User::find($member_id->user_id);
      }

      return view('teams.show')->with(['team' => $team, 'members' => $members]);
    }

    public function join($team_id)
    {
      $team = Team::where('team_id', $team_id)->first();

      return view('teams.join')->with('team', $team);
    }

    public function joined(Request $request)
    {
      TeamMember::create(
        array(
          'team_id' => $request->team_id,
          'user_id' => Auth::user()->id
        )
      );

      return view('teams.joined');
    }

    public function give($team_id)
    {
      $team = Team::where('team_id', $team_id)->first();

      return view('teams.give')->with('team', $team);
    }

    public function given(Request $request)
    {
      // 送り手のポイントを減らす
      $sender_now_point = User::find(Auth::user()->id)->points;
      $sender_after_point = $sender_now_point - $request->point;
      User::find(Auth::user()->id)->update([
        'points' => $sender_after_point
      ]);

      // 貰い手のポイントを増やす
      $team_members = TeamMember::where('team_id', $request->team_id)->get();
      foreach ($team_members as $team_member) {
        $sended_now_point = User::find($team_member->user_id)->points;
        $sended_after_point = $sended_now_point + $request->point;
        User::find($team_member->user_id)->update([
          'points' => $sended_after_point
        ]);
      }

      TeamMessage::create(
        array(
          'team_id' => $request->team_id,
          'user_id' => Auth::user()->id,
          'message' => $request->message,
          'point'   => $request->point
        )
      );

      return view('teams.given');
    }

    public function history($team_id) {
      $team_name = Team::where('team_id', $team_id)->first()->team_name;
      $team_messages = TeamMessage::where('team_id', $team_id)->get();
      foreach ($team_messages as $team_message) {
        $team_message['user_name'] = User::find($team_message->user_id)->name;
      }
      // dd($team_messages);

      return view('teams.history')->with(['team_name' => $team_name, 'team_messages' => $team_messages]);
    }
}
