<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Message;
use App\User;
use App\Point;

class MessagesController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth', array('except' => 'index'));
    }

    public function index()
    {
      $all_messages = Message::with('user')->orderBy('message_id', 'DESC')->paginate(20);
      $message_infos = [];
      foreach ($all_messages as $all_message) {
        $user_info = User::find($all_message->user_id);
        $usered_info = User::find($all_message->who_id);
        $message_infos[] = [
          'user_id'     => $user_info->id,
          'user_name'   => $user_info->name,
          'usered_id'   => $usered_info->id,
          'usered_name' => $usered_info->name,
          'message'     => $all_message->message,
          'point'       => $all_message->point
        ];
      }

      return view('messages.index')->with(["message_infos" => $message_infos]);
    }

    public function create()
    {
      $users = User::all();
      return view('messages.create')->with('users', $users);
    }

    public function store(Request $request)
    {
      // 送り手のポイントを減らす
      $sender_now_point = User::find(Auth::user()->id)->points;
      $sender_after_point = $sender_now_point - $request->point;
      User::find(Auth::user()->id)->update([
        'points' => $sender_after_point
      ]);

      // 貰い手のポイントを増やす
      $sended_now_point = User::find($request->select)->points;
      $sended_after_point = $sended_now_point + $request->point;
      User::find($request->select)->update([
        'points' => $sended_after_point
      ]);

      Message::create(
        array(
          'user_id' => Auth::user()->id,
          'who_id'  => $request->select,
          'message' => $request->message,
          'point'   => $request->point
        )
      );

      // ポイントテーブルに履歴を残す
      Point::create([
        'user_id' => Auth::user()->id,
        'who_id'  => $request->select,
        'point'   => $request->point,
      ]);

      return view('messages.store');
    }
}
