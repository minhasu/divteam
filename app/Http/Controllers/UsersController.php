<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Message;
use App\Team;
use App\TeamMember;

class UsersController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth', array('except' => 'index'));
    }

    public function index()
    {
      $users = User::orderBy('updated_at', 'DESC')->paginate(20);
      return view('users.index')->with('users' , $users);
    }

    public function create()
    {
      return view('users.create');
    }

    public function store(Request $request)
    {
      return view('users.store');
    }

    public function show($id)
    {
      $user_info = User::find($id);
      $user_messages = User::find($id)->messages()->orderBy('updated_at', 'DESC')->paginate(10);
      foreach ($user_messages as $user_message) {
        $user_message['user_name'] = User::find($user_message->user_id)->name;
        $user_message['who_name'] = User::find($user_message->who_id)->name;
      }

      $team_ids = TeamMember::where('user_id', $id)->get();
      $teams = [];
      foreach ($team_ids as $team_id) {
        $teams[] = Team::where('team_id', $team_id->team_id)->first();
      }

      return view('users.show')->with(['user_info' => $user_info, 'user_messages' => $user_messages, 'teams' => $teams]);
    }

    public function given($id)
    {
      $user_info = User::find($id);
      $given_messages = Message::where('who_id', $id)->get();
      foreach ($given_messages as $given_message) {
        $given_message['user_name'] = User::find($given_message->user_id)->name;
      }

      return view('users.given')->with(['given_messages' => $given_messages, 'user_info' => $user_info]);
    }
}
