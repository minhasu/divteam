<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\User;

class Message extends Model
{
    protected $fillable = [
      'user_id', 'who_id', 'point', 'message'
    ];

    //primaryKeyの変更
    // protected $primaryKey = "team_id";

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
